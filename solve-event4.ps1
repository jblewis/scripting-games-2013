﻿# Returns 20 random users from AD, displaying SamAccountName 'username', Dpeartment, Title, LastLogonDate (Date), PasswordLastSet (Date), Enabled (Bool), LockedOut (Bool)
# Requires PowerShell 3.0 to automatically import ActiveDirectory Module

Get-ADUser -filter * -Properties Department, Title, LastLogonDate, PasswordLastSet, Enabled, LockedOut |
Get-Random -Count 20 | 
select @{Name="UserName";Expression = {$_.samaccountname}}, Department, Title, LastLogonDate, PasswordLastSet, Enabled, LockedOut |
ConvertTo-Html -as Table -Title "User Account Audit" -PreContent "<H2>User Account Audit</H2>" -PostContent "<HR> Retrieved: $(Get-Date -Format G)"  |
Out-File -FilePath c:\support\audit.html